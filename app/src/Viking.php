<?php
namespace Tournament;

class Viking extends Warrior
{
    private $hitpoints = 120;
    private $damage = 6;
    private $equipment = [];
    private $weapon = "AXE";
    private $block = true;
    private $blocked = 0;
    private $isVicious = false;

    public function hitpoints() {
        return $this->hitpoints;
    }

    public function damage() {
        return $this->damage;
    }

    public function setHealth($hitpoints) {
        $this->hitpoints = $hitpoints;
    }

    public function getWeapon() {
        return $this->weapon;
    }

    public function hasBuckler() {
        return in_array('buckler', $this->equipment);
    }

    public function equip($equipment) {
        array_push($this->equipment, $equipment);
        return $this;
    }

    public function weapon() {
        return $this->weapon;
    }

    public function stopOrDestroy($enemy) {
        $equipment = [];
        $destroyed = false;
        if($enemy->weapon() == "AXE") {
            $equipment = array_values(array_filter($this->equipment, function($m) {
                return $m != "buckler";
           }));
           $destroyed = true;
        }

        if($destroyed) {
            foreach($equipment as $item) {
                $this->equip($item);
            }
        }

    }

    public function setBlock($value) {
        $this->block = $value;
    }

    public function getBlock() {
        return $this->block;
    }

    public function setBlocked($value) {
        $this->blocked = $value;
    }

    public function getBlocked() {
        return $this->blocked;
    }

    public function className() {
        return get_class($this);
    }

    public function hasArmor() {
        return in_array('armor', $this->equipment);
    }

    public function isVicious() {
        return $this->isVicious;
    }
}