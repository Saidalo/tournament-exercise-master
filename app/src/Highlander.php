<?php
namespace Tournament;

class Highlander extends Warrior
{
    private $hitpoints = 150;
    private $initialTotalHP = 150;
    private $damage = 12;
    private $berserk = 24;
    private $isVeteran = false;
    private $equipment = [];
    private $weapon = "GreatSword";
    private $blow = 0;
    private $isVicious = false;

    function __construct($warriorType="Unit") {
        if($warriorType == "Veteran") {
            $this->isVeteran = true;
        }
        $this->warriorType = $warriorType;
    }

    public function hitpoints() {
        return $this->hitpoints;
    }

    public function damage() {
        return $this->damage;
    }

    public function setHealth($hitpoints) {
        $this->hitpoints = $hitpoints;
    }

    public function getWeapon() {
        return $this->weapon;
    }

    public function hasBuckler() {
        return in_array('buckler', $this->equipment);
    }

    public function equip($equipment) {
        array_push($this->equipment, $equipment);
    }

    public function weapon() {
        return $this->weapon;
    }

    public function className() {
        return get_class($this);
    }

    public function hasArmor() {
        return in_array('armor', $this->equipment);
    }

    public function oneBlow() {
        $this->blow += 1; 
    }

    public function resetBlow() {
        $this->blow = 0; 
    }

    public function getBlow() {
        return $this->blow; 
    }
    
    public function isVicious() {
        return $this->isVicious;
    }

    public function initialTotalHP() {
        return $this->initialTotalHP;
    }

    public function berserkAttack() {
        return $this->berserk;
    }

    public function isVeteran() {
        return $this->isVeteran;
    }
}