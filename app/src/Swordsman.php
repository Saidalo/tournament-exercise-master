<?php
namespace Tournament;

class Swordsman extends Warrior
{
    private $hitpoints = 100;
    private $damage = 5;
    private $equipment = [];
    private $weapon = "SWORD";
    private $block = true;
    private $blocked = 0;
    private $blow = 0;
    private $isVicious = false;

    function __construct($warriorType="Unit") {
        if($warriorType == "Vicious") {
            $this->isVicious = true;
        }
        $this->warriorType = $warriorType;
    }

    public function engage($enemy) {
        do {
            $warriorState = $this->handToHandCombat($this, $enemy);
            $this->setHealth($warriorState['warrior']->hitpoints());
            $enemy->setHealth($warriorState['opponent']->hitpoints());
        } while(($enemy->hitpoints() > 0) && ($this->hitpoints() > 0));
    }

    public function hitpoints() {
        return $this->hitpoints;
    }

    public function damage() {
        return $this->damage;
    }

    public function setHealth($hitpoints) {
        $this->hitpoints = $hitpoints;
    }

    public function hasBuckler() {
        return in_array('buckler', $this->equipment);
    }

    public function hasArmor() {
        return in_array('armor', $this->equipment);
    }

    public function equip($equipment) {
        if(in_array(strtoupper($equipment), $this->weapons())) {
            $this->weapon = $equipment;
            $this->damage = $equipment == "axe" ? 6 : $this->damage;
            return $this;
        }
        array_push($this->equipment, $equipment);
        return $this;
    }

    public function weapon() {
        return $this->weapon;
    }

    public function stopOrDestroy($enemy) {
        $equipment = [];
        $destroyed = false;
        if($enemy->weapon() == "AXE") {
            $equipment = array_values(array_filter($this->equipment, function($m) {
                return $m != "buckler";
           }));
           $destroyed = true;
        }

        if($destroyed) {
            $this->equipment = $equipment;
        }
    }

    public function setBlock($value) {
        $this->block = $value;
    }

    public function getBlock() {
        return $this->block;
    }

    public function setBlocked($value) {
        $this->blocked = $value;
    }

    public function getBlocked() {
        return $this->blocked;
    }

    public function getEquipment() {
        return $this->equipment;
    }

    public function className() {
        return get_class($this);
    }
    public function oneBlow() {
        if ($this->blow >= 1) {
            $this->isVicious = false;     //Disabling poison of vicious attacker after second blow
        }
        $this->blow += 1; 
    }

    public function getBlow() {
        return $this->blow; 
    }

    public function isVicious() {
        return $this->isVicious;
    }
}
