<?php
namespace Tournament;

class Warrior 
{
    private $block = true;
    private $blocked = 0;
    private $warriorType = "Unit";
    private $weapons = ["AXE", "SWORD"];

    public function handToHandCombat($warrior, $opponent) {
        $opponent = $this->hit($warrior, $opponent);
        $warrior = $this->hit($opponent, $warrior);
        return [
            'warrior' => $warrior,
            'opponent' => $opponent
        ];
    }

    public function isDead($hitpoints) {
        return $hitpoints<=0;
    }

    public function hit($attacker, $defender) {
        if($defender->hasBuckler() && $defender->getBlocked() == 3) {
            $defender->stopOrDestroy($attacker);
            $defender->setBlocked(0);
        }

        $damage = $attacker->hasArmor() ? ($attacker->damage() - 1) : $attacker->damage();

        if ($attacker->isVicious()) {
            $damage = $damage + 20;                 //Poisonous blow
            $attacker->oneBlow();
        }

        if($attacker->className() == Highlander::class) {
            if($attacker->getBlow() == 2) {
                $attacker->resetBlow();
                return $defender;
            } else {
                $attacker->oneBlow();
            }
            $attackerDamage = $attacker->damage();
            if($attacker->hitpoints() < ($attacker->initialTotalHP() * 0.3) &&
                $attacker->isVeteran()) {
                $attackerDamage = $attacker->berserkAttack();
            }
            $damage = $defender->hasArmor() ? ($attackerDamage - 3) : $attackerDamage;
        }



        if ($defender->hasBuckler() && $defender->getBlock() ) {
            $defender->setBlock(false);
            $defender->setBlocked($defender->getBlocked() + 1);
            return $defender;
        }
 

        $defender->setHealth(
            $this->isDead($defender->hitpoints() - $damage) ?
            0 : ($defender->hitpoints() - $damage)
        );
        
        if ($defender->hasBuckler()) {
            $defender->setBlock(true);
        }
        return $defender;
    }

    public function weapons() {
        return $this->weapons;
    }
}